cmake_minimum_required(VERSION 3.8)
project(SatisfactorySDK VERSION 0.103 LANGUAGES CXX)

include(GNUInstallDirs)

set(TARGET_NAME satisfactorysdk)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

set(SOURCES SDK/SF_Basic.cpp SDK/SF_CoreUObject_functions.cpp SDK/SF_Engine_functions.cpp SDK/SF_FactoryGame_functions.cpp)

add_library(${TARGET_NAME} STATIC SDK.hpp ${SOURCES})

if (MSVC)
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MD")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MD")
endif()

